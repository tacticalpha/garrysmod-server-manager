#!/bin/bash
INSTANCE='../instance'

args=("$@")
argstr=""
# for i in $(seq 0 2 $(($# - 1))); do
#   echo $i
#   echo "${args[$i]}"
#   echo "${args[$(($i + 1))]}"
#   argstr=$argstr" "${args[$i]}" \"${args[$(($i + 1))]}\""
# done
for arg; do
  argstr=$argstr" \"$arg\""
done
# echo $argstr
fish -c "cd garrysmod-serverman/; python3 main.py --target-folder ../instance $argstr"
