
import requests
import sqlite3
import re
import os

import g

def get_metadata(ids):
    body = {
        'itemcount': len(ids)
    }
    for i, id in enumerate(ids):
        body[f'publishedfileids[{i}]'] = id

    res = requests.post(
        'https://api.steampowered.com/ISteamRemoteStorage/GetPublishedFileDetails/v1/?key=EC016B748250B3978ECEB38153EBFD88',
        data=body
    )

    metadata = {}
    response = res.json()['response']
    for item in response['publishedfiledetails']:
        metadata[item['publishedfileid']] = item

    return metadata

def download(ctx):
    ids = ctx.args
    script = ('force_install_dir {}\n'.format(g.state['target_folder'])
            + 'login anonymous\n')

    for id in ids:
        script += 'workshop_download_item 4020 {} validate\n'.format(id)

    script += 'quit'
    script_path = os.path.join(g.state['target_folder'], 'cache', 'steamcmd_download_script')
    with open(script_path, 'w+') as f:
        f.write(script)

    metadata = {}
    try:
        metadata = get_metadata(ids)
    except Exception as e:
        print('ERRORR!!!!!! I COULDNT GET THE META DATATAATATATA ;-;')
        print(e)

    sets = []
    for id in ids:
        if id not in metadata:
            continue
        meta = metadata[id]
        tags = []
        for tag in meta['tags']:
            tags.append(tag['tag'])
        tags = ','.join(tags)

        sets.append((id, 1, meta['title'], meta['time_updated'], tags))

    conn = sqlite3.connect(os.path.join(g.state['target_folder'], 'addons.sqlite3'))
    cursor = conn.cursor()
    cursor.executemany('INSERT INTO addons VALUES (?, ?, ?, ?, ?)', sets)
    conn.commit()
    conn.close()

    os.system(f'steamcmd +runscript "{script_path}"')

    addon_dir = os.path.join(
        g.state['target_folder'],
        'steamapps',
        'workshop',
        'content',
        '4000'
    )

    try:
        os.symlink(
            addon_dir,
            os.path.join(g.state['target_folder'], 'addons')
        )
    except FileExistsError:
        pass

    # TODO: Check this for all addons
    for id in ids:
        if id not in metadata:
            continue

        clean_title = metadata[id]['title'].lower().replace(' ', '_')

        noext = os.path.join(addon_dir, '{}'.format(id))
        files = os.listdir(noext)
        regexp = re.compile('.+\.gma')
        for fn in files:
            if re.match(fn):
                try:
                    os.symlink(os.path.join(noext, fn), f'{noext}-{clean_title}.gma')
                except FileExistsError:
                    pass
                finally:
                    break
