from os.path import join
import shutil
import os

from classes import Namespace

import persist
import g

def config_load(ctx):
    if ctx.helpers.assert_running():
        return

    name = ctx.args[0]

    # symlink_targets = [
    #     (join(ctx.config_dir, 'generated_workshop.lua'))
    # ]

    print(f'Loading config {g.esc.bold}{name}{g.esc.normal}')

    config_target = join(g.state.server_dir, 'garrysmod', 'cfg', 'current_config_settings.cfg')
    if os.path.isfile(config_target):
        os.remove(config_target)

    os.symlink(
        join(ctx.config_dir, 'settings.cfg'),
        config_target
    )

    workshop_file = join(
        g.state.server_dir,
        'garrysmod',
        'lua',
        'autorun',
        'server',
        'gsm_workshop.lua'
    )
    if not os.path.islink(workshop_file) and os.path.isfile(workshop_file):
        shutil.copyfile(workshop_filez, f'{workshop_file}.bak')

    os.remove(workshop_file)
    os.symlink(
        join(ctx.config_dir, 'generated_workshop.lua'),
        workshop_file
    )

    addon_folder = join(g.state.dirs.gmod_server, 'cache', 'srcds')
    if os.path.islink(addon_folder):
        os.remove(addon_folder)
    elif os.path.isdir(addon_folder):
        shutil.move(addon_folder, addon_folder + '.bak')

    os.symlink(
        join(ctx.config_dir, 'srcds_cached_addons'),
        addon_folder,
        target_is_directory=True,
    )

    print(f'Config loaded :)')

    persist.set('current_config', name)
    g.state.dirs.current_config = ctx.config_dir

    # with open(join(g.state.target_folder, 'cache', ))

    # regexp = re.compile('^([0-9]+).*\.gma$')
    # fn_list = map(
    #     lambda fn: regexp.match(fn),
    #     os.listdir(join(g.state['target_folder'], 'addons'))
    # )
    # addons = { fn.group(1): fn.string for fn in fn_list if fn is not None }
    #
    # addons_dir = join(ctx.server_dir, 'addons')
    # os.rmdir(addons_dir)
    # os.mkdir(addons_dir)
    #
    # line_re = re.compile('^([0-9]+)')
    # with open(join(config_dir, 'addon_list.txt'), 'r') as f:
    #     for line in f:
    #         line = line.strip()
    #         match = line_re.match(line)
    #         if not match:
    #             continue
    #         id = match.group(1)
    #         os.symlink(addons[id], join(addons_dir))
config_load.help = 'set server to use specific config'
config_load.args = '<name>'
