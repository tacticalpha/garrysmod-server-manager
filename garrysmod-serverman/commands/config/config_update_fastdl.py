from os.path import join
import os
import re

from classes import Namespace

import persist
import g


def config_update_fastdl(ctx):
    addon_files = os.listdir(
        join(
            g.state.server_dir,
            'garrysmod',
            'cache',
            'srcds'
        )
    )
    regexp = re.compile('^([0-9]+)\.gma')
    addons = [
        match.group(1)
        for match in map(regexp.match, addon_files)
        if match is not None
    ]
    # fn = join(
    #     g.state.server_dir,
    #     'garrysmod',
    #     'lua',
    #     'autorun',
    #     'server',
    #     'gsm_workshop.lua'
    # )

    fn = join(
        g.state.dirs.current_config,
        'generated_workshop.lua'
    )

    new_addons_n = len(addons)

    current_addons_n = 0
    regexp = re.compile('^resource.AddWorkshop\("[0-9]+"\)')
    if os.path.isfile(fn):
        with open(fn, 'r') as f:
            for line in f:
                if regexp.match(line):
                    current_addons_n += 1

    diff = new_addons_n - current_addons_n

    print(f'updating lua/autorun/server/gsm_workshop.lua')
    print(f'{new_addons_n} addons ({"+" if diff >= 0 else "-"}{diff})')

    with open(fn, 'w+') as f:
        f.write('-- Changes here won\'t be saved when `update_fastdl` is run again\n')
        # f.write('-- If you want to add custom workshop resources create a new file\n')
        # f.write('-- in this folder, maybe workshop.lua?\n')
        f.write('\n')
        f.write('print("garrysmod-serverman workshop file executing...")\n')

        for id in addons:
            f.write(f'resource.AddWorkshop("{id}")\n')

        f.write('-- end')

    print('done :)')
config_update_fastdl.help = 'Update\'s workshop fastdl file for clients.'
