
from os.path import join
import os

from classes import Namespace

import persist
import g

def config_copy(ctx):
    target = join(ctx.configs_dir, ctx.args[0])
    new    = join(ctx.configs_dir, ctx.args[1])

    cmd = f'cp -R "{target}" "{new}"'
    print('$', cmd)
    os.system(cmd)


config_copy.help = 'copy an existing config'
config_copy.args = '<target> <new name>'
