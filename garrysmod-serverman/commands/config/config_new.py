
from os.path import join
import os

from classes import Namespace

import persist
import g

def config_new(ctx):
    name = ctx.args[0]

    configs_dir = os.path.join(g.state['target_folder'], 'configs')
    dir = os.path.join(configs_dir, name)
    os.mkdir(dir)

    files = [
        'addon_list.txt',
        'settings.cfg',
        'command_arguments.txt'
    ]
    for fn in files:
        f = open(os.path.join(dir, fn), 'a+')
        f.write('')

    dirs = [
        'srcds_cached_addons'
    ]
    for fn in dirs:
        os.mkdir(os.path.join(dir, fn))

config_new.help = 'create new server config in $INSTANCE/configs'
config_new.args = '<name>'
# config_new.args = [
#     {
#         'name': 'name',
#         'required': True
#     }
# ]
