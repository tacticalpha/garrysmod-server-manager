from os.path import join
import os

from classes import Namespace

import persist
import g

def config_refresh_workshop(ctx):
    if ctx.helpers.assert_running():
        return

    g.handle_command(
        'start',
        internal_kwargs = {
            'after_args': '+quit'
        }
    )
    g.handle_command(
        'connect',
        internal_kwargs = {
            'no_warning': True
        }
    )
config_refresh_workshop.help = ('runs the server and immediately quits to refresh workshop collection\n'
                               + '  use this and then `config update_fastdl`')
