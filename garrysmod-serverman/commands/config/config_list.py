from os.path import join
import os

from classes import Namespace

import persist
import g

def config_list(ctx):
    configs = [
        fn
        for fn in os.listdir(ctx.configs_dir)
        if os.path.isdir(join(ctx.configs_dir, fn))
    ]

    for config in configs:
        print(config)

config_list.help = 'list all config folders'
