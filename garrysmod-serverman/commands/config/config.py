
from os.path import join
import os
import re

from classes import Namespace

import persist
import screen
import g

from .config_copy import config_copy
from .config_help import config_help
from .config_load import config_load
from .config_list import config_list
from .config_new import config_new
from .config_refresh_workshop import config_refresh_workshop
from .config_update_fastdl import config_update_fastdl

subcommands = {}
subcommands['copy']             = config_copy
subcommands['help']             = config_help
subcommands['load']             = config_load
subcommands['list']             = config_list
subcommands['new']              = config_new
subcommands['refresh_workshop'] = config_refresh_workshop
subcommands['update_fastdl']    = config_update_fastdl

def helper_assert_running(msg=None):
    status = screen.is_running()
    if status:
        if msg is None:
            print('Sorry, you can\'t run this while the server\'s running :(')
            print('Use `stop` to stop it first')
        return True

    return False

helpers = Namespace(
    assert_running=helper_assert_running
)

def config(ctx):
    args    = ctx.args[1:]
    command = ctx.args[0] if ctx.args else None

    if command is None or command not in subcommands:
        subcommands['help'](subcommands=subcommands)
    else:
        name = args[0] if len(args) >= 1 else None

        configs_dir = join(g.state['target_folder'], 'configs')
        config_dir  = join(configs_dir, name) if name else None
        if config_dir and not os.path.isdir(config_dir):
            print('no config folder with that name :(')
            return 1

        ctx = Namespace(
            helpers     = helpers,
            configs_dir = configs_dir,
            config_dir  = config_dir,
            args        = args,
            name        = name,
        )
        subcommands[command](ctx)

config.help = 'Command to manage set, manage, and create configs. Lists all availible subcommands'
