from os.path import join
import os

from classes import Namespace

import persist
import g

def config_help(ctx=None, *, subcommands=None):
    print('Config subcommands:')
    for name in subcommands:
        cmd = subcommands[name]
        line = f'  {name}'
        if hasattr(cmd, 'args'):
            line += f' {cmd.args}'

        if hasattr(cmd, 'help') and cmd.help:
            help_str = cmd.help.replace("\n", "\n  ")
            line += f' - {help_str}'

        print(line)
