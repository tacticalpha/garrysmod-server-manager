
import os

import persist
import g

targets = {
    'settings': 'settings.cfg',
    'args': 'command_arguments.txt'
}

def edit(ctx):
    target = (ctx.args[0] or 'list').lower()
    editor = os.environ.get("EDITOR")
    if target == 'list' or target not in targets:
        print(f'Current $EDITOR: {editor}')
        print('edit targets:')
        for name in targets:
            print(f'  {name} => {targets[name]}')
    else:
        if editor is None:
            print('$EDITOR environment variable is unset')
            print('Which editor would you like to use?')
            editor = input('[nano] > ')
            if not editor or editor == '':
                editor = 'nano'

        fn = os.path.join(
            g.state.target_folder,
            "configs",
            persist.get('current_config'),
            targets[target]
        )
        os.system(
            f'bash -c "{editor} \"{fn}\""'
        )


edit.help = 'a shortcut to current config files'
