from os.path import join
import time
import os

import screen
import g

def log(ctx):
    log_file = join(g.state.dirs.cache, 'latest.log')

    if screen.is_running():
        print('Server is currently running, press ctrl-c to stop vewing the log')
        time.sleep(1)
        os.system(f'tail -F "{log_file}"')
        print()
    else:
        os.system(f'cat "{log_file}"')

log.help = 'Get latest server logs'
