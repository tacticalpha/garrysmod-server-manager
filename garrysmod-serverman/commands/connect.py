
import os

import screen
import g



def connect(ctx):
    if not screen.is_running():
        print('server isn\'t running :(')
        print('start it first with `start`')
        return 1

    if 'no_warning' not in ctx.internal_kwargs:
        print('Opening server console')
        print('To close it without stopping the server, press Ctrl+A then D')
        print('Ctrl+C or Ctrl+D will stop the server')
        print()
        input('Press enter to continue')

    os.system(
        'screen -r garrysmod-serverman'
    )

    from functions import print_welcome
    # print(print_welcome, dir(print_welcome))
    print()
    print_welcome()
    # print(f'{g.esc.clear}welcome back to gsm')


connect.help = 'open server console if it\'s running'
