
from .config import config
from .connect import connect
from .edit import edit
from .download import download
from .log import log
from .status import status
from .start import start
from .stop import stop
from .steamcmd import steamcmd
from .update import update

commands = {
    'config'     : config,
    'connect'    : connect,
    'edit'       : edit,
    #'download' : download,
    'log' : log,
    'status'     : status,
    'start'      : start,
    'stop'      : stop,
    # 'steamcmd' : steamcmd,
    'update'     : update,
}
