
import subprocess

import persist
import screen
import g

def status(ctx):
    state = screen.is_running()
    server = 'up' if state else 'down'
    config = persist.get('current_config')

    welcome = 'welcome_msg' in ctx.internal_kwargs and ctx.internal_kwargs['welcome_msg']
    if welcome and state:
        print()

    server_before = g.esc.bold
    if 'no_color' in ctx.internal_kwargs:
        pass
    elif state:
        server_before += g.esc.green
    else:
        server_before += g.esc.red

    print(
        f'server is {server_before}{server}{g.esc.normal}'
        f' currently using config {g.esc.bold}{config}{g.esc.normal} '
    )
    if welcome and state:
        print('type `connect` to access it')
