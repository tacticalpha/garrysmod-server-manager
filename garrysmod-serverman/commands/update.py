import os.path

import argparse

import g

def update(ctx):
    # print('heyyyy')
    # print(
    #     'bash {} {}'.format(
    #         os.path.join(g.state['script_dir'], 'bin/update_gmod.sh'),
    #         g.state['target_folder']
    #     )
    # )
    # return

    script_path = os.path.join(g.state['target_folder'], 'cache', 'steamcmd_update_script')

    server_install_dir = os.path.join(g.state.target_folder, 'server')
    content_install_dir = os.path.join(g.state.target_folder, 'content')
    apps = [
        ('app_update 4020 validate', server_install_dir),
        ('app_update 232250 validate', content_install_dir),
        ('app_update 232330 validate', content_install_dir),
    ]
    for install_cmd, dir in apps:
        script = '\n'.join([
            'force_install_dir {}'.format(dir),
            'login anonymous',
            install_cmd,
            'quit'
        ])
        with open(script_path, 'w+') as f:
            f.write(script)

        os.system(
            f'cd {g.state["target_folder"]} && '
            + f'steamcmd +runscript "{script_path}"'
        )



    # os.system(
    #     'bash {} {}'.format(
    #         os.path.join(g.state['script_dir'], 'bin/update_gmod.sh'),
    #         g.state['target_folder']
    #     )
    # )

update.help = 'Update gmod install, css resources, and tf2 resources'

# def get_argparser():
#     parser = argparse.ArgumentParser()
# update.get_argparser =
