
import os

import g

def steamcmd(args):
    command = (
        'steamcmd '
        + f'+force_install_dir "{g.state["target_folder"]}" '
        + f'{" ".join(args)}'
    )
    print('$ ', command)
    os.system(command)

steamcmd.help = 'start steamcmd with correct instance install directory set'
