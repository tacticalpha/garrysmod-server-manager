
class Namespace:
    def __init__(self, *args, **kwargs):
        if len(args) == 1 and type(args[0]) == dict:
            kwargs = {**args[0], **kwargs}

        for k in kwargs:
            setattr(self, k, kwargs[k])

    def __getitem__(self, k):
        return getattr(self, k)

    def __setitem__(self, k, v):
        setattr(self, k, v)
