
class DefaultList(list):
    def __init__(self, *args, **kwargs):
        super(DefaultList, self).__init__(*args, **kwargs)
        self.__original_getitem__ = super().__getitem__

    def __getitem__(self, k):
        try:
            return self.__original_getitem__(k)
        except IndexError:
            return None
