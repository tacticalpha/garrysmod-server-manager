
this is gonna be a better description of configs at some point
idk \'-'/

addon_list.txt is a list of addon ids to symlink into the addons folder when
config is loaded. any text after the id is ignored
ex:
---addon_list.txt-----
112436123 you can type whateeverrrrr you want here
123123467
546734532
123653464
----------------------
This can be managed with `config $CONFIG_NAME addons ...`

command_args.txt is a list of command line args to be passed to ./srcds_run
arguments are automatically quoted
---command_args.txt-----
# this is a comment xd
+maxplayers 69
+gamemode terrortown
+sv_loadingurl ...
------------------------
this example turns into `+maxplayers "69" +gamemode "terrortown", +sv_loadingurl "..."`
