
import subprocess

def is_running():
    res = subprocess.run(
        'screen -list | grep garrysmod-serverman',
        stdout=subprocess.PIPE,
        shell=True
    ).stdout.decode('utf-8')

    return bool(res)
