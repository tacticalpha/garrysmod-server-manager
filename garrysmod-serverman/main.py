#!/usr/bin/python3

# GITLAB LINK HERERERERERE
from __version__ import __version__

import argparse
import sys
import os

from classes import Namespace

import g
g.state = Namespace(
    script_dir = os.path.dirname(__file__),
    dirs = Namespace(
        script_root    = os.path.dirname(__file__),
        current_config = None,
        configs        = None,
        cache          = None,
        gmod_server    = None
    )
)
g.esc = Namespace(
    bold   = '\033[1m',
    normal = '\033(B\033[m',
    clear  = '\033[H\033[2J\033[3J',
    smcup  = '\033[?1049h\033[22;0;0t',
    rmcup  = '\033[?1049l\033[23;0;0t',

    red   = '\033[31m',
    green = '\033[32m',
)


from functions import print_welcome
from functions import command_line
from functions import prompt

import persist

from structure_checks import structure_checks
from handle_command import handle_command
g.handle_command = handle_command


if sys.version_info < (3,):
    raise ('Sorry, this script was made for python 3 or above :(\n'
          + 'Try running it with python3')

try:
    import epysh
except ImportError:
    print('This script requires "epysh" (https://gitlab.com/tacticalpha/epysh) :(')
    print('You can install it with')
    print('- `python3 -m pip install git+https://gitlab.com/tacticalpha/epysh.git`')
    print()
    sys.exit(1)

# try:
#     import requests
#     import yaml
# except ImportError:
#     print('This script requires "requests" and "pyyaml"')
#     print('It looks like or or more of them are missing :(')
#     print('You can install them with')
#     print('- `python -m pip install requests pyyaml`')
#     print()
#     print('If none of these work, try googling the issue :(')
#     sys.exit(1)

parser = argparse.ArgumentParser()
parser.add_argument(
    '-t', '--target-folder',
    default='./instance',
    help='Instance folder to install server into'
)
parser.add_argument(
    '-c', '--command'
)
parser.add_argument(
    'command_string', metavar='COMMAND', type=str, nargs='*'
)
# parser.add_argument(
#     '-d', '--dry',
#     action='store_true',
#     help='Enable dry mode'
# )

args = parser.parse_args()

g.state.target_folder = os.path.abspath(args.target_folder)
g.state.server_dir    = os.path.join(g.state['target_folder'], 'server')

persist._read_()
structure_checks()

g.state.dirs.configs     = os.path.join(g.state.target_folder, 'configs')
g.state.dirs.cache       = os.path.join(g.state.target_folder, 'cache')
g.state.dirs.gmod_server = os.path.join(g.state.target_folder, 'server', 'garrysmod')

if persist.get('current_config'):
    g.state.dirs.current_config = os.path.join(
        g.state.dirs.configs,
        persist.get('current_config')
    )

cache_path = os.path.join(args.target_folder, 'config.yaml')

if args.command:
    handle_command(args.command)
    sys.exit(0)

if args.command_string:
    handle_command(' '.join(args.command_string))
    sys.exit(0)

def main():
    print_welcome()

    shell = epysh.Shell(
        prompt=prompt
    )

    while True:
        try:
            # line = input(prompt()).strip()
            line = shell.input().strip()
            # line = command_line()
        except KeyboardInterrupt:
            print()
            print('Cancelled')
            sys.exit(0)

        handle_command(line)

if __name__ == '__main__':
    main()
