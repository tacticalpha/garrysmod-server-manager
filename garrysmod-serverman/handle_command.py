
import traceback

from classes import Namespace
from classes import DefaultList

from commands import commands

import g

# command_list = list(filter(lambda i: not i.startswith('_'), dir(commands)))
# print(command_list)
def handle_command(lines, *, internal_args=DefaultList(), internal_kwargs={}):
    for line in lines.split(';'):
        split = line.strip().split()
        n = len(split)

        if n == 0:
            return 0
        elif n == 1:
            cmd = split[0]
            args = []
        else:
            cmd = split[0]
            args = split[1:]

        args = DefaultList(args)

        if cmd == 'help':
            print('Available commands:')
            for name in sorted(commands.keys()):
                if hasattr(commands[name], 'help'):
                    print(f'  {name} - {commands[name].help}')
                else:
                    print(f'  {name}')

        elif cmd in commands:
            ctx = Namespace(
                args            = args,
                internal_args   = internal_args,
                internal_kwargs = internal_kwargs
            )
            try:
                commands[cmd](ctx)
            except Exception as e:
                print()
                print("uh oh it looks like there was an internal error")
                traceback.print_exc()
                print()
                return 1
        else:
            print(f'unknown command "{cmd}":(')
            print('try "help" for a list of commands')
            return 254
