
import os.path
import re

import g

store = {}

def _get_persist_file_():
    if not hasattr(g.state, 'target_folder'):
        return None
    else:
        return os.path.join(g.state.target_folder, 'cache', 'persistant_variables.txt')

_read_lock_ = True
def _read_():
    fn = _get_persist_file_()
    if fn is None:
        print('uh oh this shouldn\'t have been run')

    new_dict = {}
    regexp = re.compile('^(.+)=(.*)')
    try:
        f = open(fn, 'r+')
    except FileNotFoundError:
        _write_()
        f = open(fn, 'r+')
        
    global _read_lock_
    if _read_lock_: _read_lock_ = False
    for line in f:
        # split = line.split('=')
        # k = split[0]
        # v = '='.join(split[1:])
        match = regexp.match(line)
        k = match.group(1)
        v = match.group(2)
        new_dict[k] = v

    for k in store.keys():
        del store[k]

    for k in new_dict.keys():
        store[k] = new_dict[k]

def _write_():
    fn = _get_persist_file_()
    if fn is None:
        print('uh oh this shouldn\'t have been run')

    with open(fn, 'w+') as f:
        for k in store.keys():
            f.write(f'{k}={store[k]}\n')

def get(k):
    if _read_lock_:
        raise Exception('persist.get called before persist._read_')

    if k not in store:
        return None
    return store[k]

def set(k, v):
    v = f'{v}'
    store[k] = v

    _write_()
