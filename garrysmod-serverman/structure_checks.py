
from os.path import join
import sqlite3
import os

import g

def structure_checks():
    mkdirs = [
        join(g.state.script_dir, '.cache'),
        g.state['target_folder'],
        join(g.state['target_folder'], 'cache'),
        join(g.state['target_folder'], 'configs'),
        join(g.state['target_folder'], 'content'),
        join(g.state['target_folder'], 'server'),
    ]
    for dir in mkdirs:
        if not os.path.isdir(dir):
            os.mkdir(dir)

    config_path = join(g.state['target_folder'], 'config.yaml')
    if not os.path.isfile(config_path):
        c  = open(config_path, 'w+')
        dc = open(join(g.state['script_dir'], 'data/default-config.yaml'), 'r')
        f.write(dc.read())

    configs_readme_path = join(mkdirs[2], 'readme.txt')
    if not os.path.isfile(configs_readme_path):
        f = open(configs_readme_path, 'w+')
        src = open(join(g.state['script_dir'], 'data/configs-readme.txt'), 'r')
        f.write(src.read())

    addon_db_path = os.path.join(g.state['target_folder'], 'addons.sqlite3')
    if not os.path.isfile(addon_db_path):
        conn = sqlite3.connect(addon_db_path)
        cursor = conn.cursor()

        # states
        #  0 - ready
        #  1 - downloading
        #  2 - couldn't download

        cursor.execute(
            'CREATE TABLE addons ('
            + 'id TEXT, '
            + 'state INT, '
            + 'title TEXT, '
            + 'time_updated INTEGER, '
            + 'tags TEXT'
            + ')'
        )
        conn.commit()
        conn.close()
