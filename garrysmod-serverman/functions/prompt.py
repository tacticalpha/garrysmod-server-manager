
import screen

import g

def prompt():
    out = f'{g.esc.bold}'

    state = screen.is_running()
    if state:
        # out += f'{g.esc.green}✓'
        out += f'{g.esc.green}  up'
    else:
        # out += f'{g.esc.red}𐄂'
        out += f'{g.esc.red}down'

    out += f' >{g.esc.normal} '
    return out
