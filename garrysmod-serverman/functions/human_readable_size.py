
STEPS = [
    'B',
    'KB',
    'MB',
    'GB'
]
def human_readable_size(n):
    i = 0
    last = n
    while i < 3:
        n = n / 1024
        if n < 1:
            break
        last = n
        i += 1
    return (last, STEPS[i])
