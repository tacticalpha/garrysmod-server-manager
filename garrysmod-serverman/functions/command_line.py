
import termios
import time
import sys
import tty
import re

from functions.prompt import prompt

import g

C_RED = '\033[38;5;202m'
C_BLUE = '\033[38;5;39m'

C_COMMAND   = C_BLUE
C_SEMICOLON = C_RED

C_NORMAL = g.esc.normal

SIGINT  = '\x03'
SIGD    = '\x04'
SIGSTP  = '\x1a'
SIGQUIT = '\x1d'

BACK = '\x7f'

UP    = ['[', 'A']
DOWN  = ['[', 'B']
RIGHT = ['[', 'C']
LEFT  = ['[', 'D']

ESC = '\x1b'

groups = (
    ('semicolon', ';'),
    ('command',   '[^;]\s*[a-zA-Z]+[a-zA-Z0-9\s]*')
)
regexp_str = ''
for group in groups:
    if regexp_str: # if not empty
        regexp_str += '|'

    regexp_str += f'(?P<{group[0]}>{group[1]})'
regexp = re.compile(regexp_str)

def write(stack):
    stack = ''.join(stack)
    out = prompt()

    for match in regexp.findall(stack):
        # sys.stdout.write(str(match) + ' ')

        # semicolon = match.group('semicolon')
        semicolon = match[0]
        # command = match.group('command')
        command = match[1]

        if semicolon:
            out += f'{C_SEMICOLON};'
        elif command:
            out += re.sub('(\w+)(.*)', f'{C_COMMAND}\\1{C_NORMAL}\\2', command)

    sys.stdout.write('\n\r' + repr(out) + '\n\r' + out + '\n\n\r')
    sys.stdout.write(
        '\r'
        + out
        + '\033[0K'
    )


def command_line():
    stack = []
    esc_buf = None

    r_index = 0

    try:
        fd = sys.stdin.fileno()
        original_settings = termios.tcgetattr(fd)

        tty.setraw(fd)
        while True:
            # sys.stdout.write(f'\r{"".join(stack)}\033[0K')
            write(stack)
            char = sys.stdin.read(1)
            # print(f'\r{repr(char)}', esc_buf is not None, char == ESC, esc_buf)

            if char in ('\x03', '\x04', '\x1a', '\x1f'):
                raise KeyboardInterrupt()

            elif esc_buf is not None:
                esc_buf.append(char)

                if esc_buf == LEFT:
                    esc_buf = None
                    sys.stdout.write('\033[2D')
                    if r_index < len(stack):
                        r_index += 1

                elif esc_buf == RIGHT:
                    esc_buf = None
                    if r_index > 0:
                        r_index -= 1

                elif len(esc_buf) > 2:
                    print('Unhandled escape code:', repr(''.join(esc_buf)))
                    esc_buf = None

            elif char == ESC:
                esc_buf = []

            elif char == '\r':
                return ''.join(stack)

            elif char == BACK:
                if stack:
                    stack.pop()
            else:
                stack.append(char)

    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, original_settings)
        print('reset!')
