
import sys

from __version__ import __version__

from handle_command import handle_command
import g

def print_welcome():
    print(
        'garrysmod serverman v{0} - python {1[0]}.{1[1]}.{1[2]} {1[3]}'.format(
            __version__,
            sys.version_info
        )
    )
    print('https://gitlab.com/tacticalpha/garrysmod-server-manager')
    print('current instance folder:', g.state.target_folder)

    handle_command("status", internal_kwargs={'welcome_msg': True})
